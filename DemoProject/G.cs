﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Web;

namespace DemoProject
{
    public static class G
    {
        public static void WriteExcelWithEpplus(string fileName, DataTable dt, string sheetName
, List<string> dateColumns = null)
        {
            if (dateColumns == null) dateColumns = new List<string>();
            ExcelPackage pck = new ExcelPackage();
            ExcelWorksheet ws = pck.Workbook.Worksheets.Add(sheetName);
            ws.Cells["A1"].LoadFromDataTable(dt, true);

            for (int i = 0; i < dt.Columns.Count; i++)
            {
                ExcelColumn excelColumn = ws.Column(i + 1);
                DataColumn dtColumn = dt.Columns[i];

                if (dtColumn.DataType == typeof(DateTime))
                    excelColumn.Style.Numberformat.Format = "dd/mm/yyyy hh:mm:ss";
                else if (dtColumn.DataType == typeof(TimeSpan))
                    excelColumn.Style.Numberformat.Format = "hh:mm:ss";
                else if (dtColumn.DataType == typeof(decimal) || dtColumn.DataType == typeof(double))
                    excelColumn.Style.Numberformat.Format = "#,##0.00";
            }

            foreach (string dateColumn in dateColumns)
            {
                int columnOrdinal = dt.Columns[dateColumn].Ordinal + 1;
                ExcelColumn excelColumn = ws.Column(columnOrdinal);
                excelColumn.Style.Numberformat.Format = "dd/mm/yyyy";
            }

            for (int i = 1; i <= ws.Dimension.Columns; i++)
            {
                ws.Column(i).AutoFit();
            }
            // return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", excelName);
            //HttpContext.Current.Response.Clear();
            //HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            //HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName));
            //HttpContext.Current.Response.BinaryWrite(pck.GetAsByteArray());
            //HttpContext.Current.Response.End();
        }

        public static DataTable ToDataTable<T>(this IList<T> data)
        {
            PropertyDescriptorCollection properties =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }
    }
}