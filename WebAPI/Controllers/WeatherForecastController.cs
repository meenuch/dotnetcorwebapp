﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public List<SampleData> Get()
        {
            //var rng = new Random();
            //return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            //{
            //    Date = DateTime.Now.AddDays(index),
            //    TemperatureC = rng.Next(-20, 55),
            //    Summary = Summaries[rng.Next(Summaries.Length)]
            //})
            //.ToArray();
            List<SampleData> data = new List<SampleData>();
            data.Add(new SampleData { ID = 183, User = "John Doe", Date = "11-7-2014", Status = "Approved", Reason = "Bacon ipsum dolor sit amet salami venison chicken flank fatback doner." });
            data.Add(new SampleData { ID = 183, User = "John Doe", Date = "11-7-2014", Status = "Approved", Reason = "Bacon ipsum dolor sit amet salami venison chicken flank fatback doner." });
            data.Add(new SampleData { ID = 183, User = "John Doe", Date = "11-7-2014", Status = "Approved", Reason = "Bacon ipsum dolor sit amet salami venison chicken flank fatback doner." });
            data.Add(new SampleData { ID = 183, User = "John Doe", Date = "11-7-2014", Status = "Approved", Reason = "Bacon ipsum dolor sit amet salami venison chicken flank fatback doner." });
            data.Add(new SampleData { ID = 183, User = "John Doe", Date = "11-7-2014", Status = "Approved", Reason = "Bacon ipsum dolor sit amet salami venison chicken flank fatback doner." });
            return data;
        }

        [HttpPost]
        public ActionResult Post()
        {
            //var rng = new Random();
            //return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            //{
            //    Date = DateTime.Now.AddDays(index),
            //    TemperatureC = rng.Next(-20, 55),
            //    Summary = Summaries[rng.Next(Summaries.Length)]
            //})
            //.ToArray();
            List<SampleData> data = new List<SampleData>();
            data.Add(new SampleData { ID = 183, User = "John Doe", Date = "11-7-2014", Status = "Approved", Reason = "Bacon ipsum dolor sit amet salami venison chicken flank fatback doner." });
            data.Add(new SampleData { ID = 183, User = "John Doe", Date = "11-7-2014", Status = "Approved", Reason = "Bacon ipsum dolor sit amet salami venison chicken flank fatback doner." });
            data.Add(new SampleData { ID = 183, User = "John Doe", Date = "11-7-2014", Status = "Approved", Reason = "Bacon ipsum dolor sit amet salami venison chicken flank fatback doner." });
            data.Add(new SampleData { ID = 183, User = "John Doe", Date = "11-7-2014", Status = "Approved", Reason = "Bacon ipsum dolor sit amet salami venison chicken flank fatback doner." });
            data.Add(new SampleData { ID = 183, User = "John Doe", Date = "11-7-2014", Status = "Approved", Reason = "Bacon ipsum dolor sit amet salami venison chicken flank fatback doner." });
            return Ok(data);
        }
    }
}