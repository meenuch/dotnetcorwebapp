﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using DemoProject.Models;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading;
using OfficeOpenXml;
using System.IO;

namespace DemoProject.Controllers
{
    public class HomeController : Controller
    {
        private static HttpClient client = new HttpClient();

        private readonly ILogger<HomeController> _logger;

        public List<SampleData> datas
        {
            get;
            set;
        }

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public async Task<IActionResult> Index()
        {
            List<SampleData> product = new List<SampleData>();
            var response = await client.GetStringAsync("http://localhost:55118/weatherforecast");
            //if (response.IsSuccessStatusCode)
            //{
            //    var a  = await response.Content.ReadAsAsync<List<SampleData>>();
            //}
            //return View();

            datas = new List<SampleData>();
            datas = JsonConvert.DeserializeObject<List<SampleData>>(response);
            // datas.Add(new SampleData { ID = 183, User = "John Doe", Date = "11-7-2014", Status = "Approved", Reason = "Bacon ipsum dolor sit amet salami venison chicken flank fatback doner." });
            Result allData = new Result() { data = datas };
            return View(allData);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public async Task<IActionResult> Excel()
        {
            List<SampleData> product = new List<SampleData>();
            var response = await client.GetStringAsync("http://localhost:55118/weatherforecast");
            datas = new List<SampleData>();
            datas = JsonConvert.DeserializeObject<List<SampleData>>(response);
            Result allData = new Result() { data = datas };
            // query data from database
            await Task.Yield();
            //        var list = new List<UserInfo>()
            //{
            //    new UserInfo { UserName = "catcher", Age = 18 },
            //    new UserInfo { UserName = "james", Age = 20 },
            //};
            var stream = new MemoryStream();

            using (var package = new ExcelPackage(stream))
            {
                var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                workSheet.Cells.LoadFromCollection(datas, true);
                package.Save();
            }
            stream.Position = 0;
            string excelName = $"UserList-{DateTime.Now.ToString("yyyyMMddHHmmssfff")}.xlsx";

            //return File(stream, "application/octet-stream", excelName);
            return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", excelName);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}