﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Controllers
{
    public class SampleData
    {
        public int ID { get; set; }
        public string User { get; set; }
        public string Date { get; set; }
        public string Status { get; set; }
        public string Reason { get; set; }
    }
}